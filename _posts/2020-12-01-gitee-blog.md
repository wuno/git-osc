---
layout: post
title: "CopyCat 增加对  Swift、Python、C++ 语言的支持"
---

<p>CopyCat 又又又更新啦，新版本增加对&nbsp;Swift、Python 和 C++ 语言的支持，现在总共支持了 8 种编程语言。</p>

<p>看看实际的效果先：</p>

<p>Swift：</p>

<p><img alt="swift" src="https://images.gitee.com/uploads/images/2020/1201/135842_4d13c5ba_8125731.png" /></p>

<p>Python：</p>

<p><img alt="Python" src="https://images.gitee.com/uploads/images/2020/1201/135855_e07eab4e_8125731.png" /></p>

<p>C++：</p>

<p><img alt="C++" src="https://images.gitee.com/uploads/images/2020/1201/135908_1eaf5b40_8125731.png" /></p>

<p>什么？你还不知道 CopyCat 是干嘛的？</p>

<p><span style="color:#c0392b"><strong>就是用来检查别人的项目抄袭了你多少代码 :D</strong></span></p>

<p>快去&nbsp;<a href="https://copycat.gitee.com/" target="_blank">https://copycat.gitee.com</a>&nbsp;看看吧！</p>
